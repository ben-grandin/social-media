const Joi = require("@hapi/joi");

module.exports = Joi.object({
	handle: Joi.string()
		.alphanum()
		.min(3)
		.max(30)
		.required(),
	password: Joi.string().required(),
	confirmPassword: Joi.ref('password'),
	email: Joi.string()
		.email().required()
});

