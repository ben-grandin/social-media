const Joi = require("@hapi/joi");

module.exports = Joi.object({
	password: Joi.string().required(),
	email: Joi.string()
		.email().required()
});

