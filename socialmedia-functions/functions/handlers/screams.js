const { db } = require("../utils/admin");


module.exports.getAllScreams = (req, res) => {
	db
		.collection("screams")
		.orderBy("createdAt", "desc")
		.get()
		.then(data => {
			let screams = [];
			data.forEach(doc => {
				screams.push(Object.assign({ screamId: doc.id, ...doc.data() }),
				);
			});
			return res.json(screams);
		}).catch(err => console.error(err));

};

module.exports.postOneScream = (req, res) => {
	const newScream = {
		...req.body,
		createdAt: new Date().toISOString(),
	};

	db
		.collection("screams")
		.add(newScream)
		// eslint-disable-next-line promise/always-return
		.then(doc => {
			res.json({ message: `document ${doc.id} has been created` });
		})
		.catch(err => {
			res.status(500).json({ err });
			console.error(err);
		});
};

