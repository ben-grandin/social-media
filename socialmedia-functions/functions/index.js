const functions = require("firebase-functions");
const app = require("express")();
const { getAllScreams, postOneScream } = require('./handlers/screams');
const { signup, login } = require("./handlers/users");


// ToDo: Add FbAuth

// Scream route
app.get("/screams", getAllScreams);
app.post("/screams", postOneScream);


// Users route
app.post("/signup", signup);
app.post("/login", login);


exports.api = functions.region("europe-west1").https.onRequest(app);